Project Proposal
https://docs.google.com/document/d/1025FIFxjieEJHTA3Q2HQLqKpYMv7pgQ2Gyz0xj2P-uE/edit?usp=sharing

Identity solution based on article : http://www.asp.net/identity/overview/extensibility/implementing-a-custom-mysql-aspnet-identity-storage-provider 
Implemented a MYSQL store for ASP.NET Identity updated to use ASP.NET Identity 2.0

main project in SuperSite / SuperSite /

live demo: http://ssdev-env.us-west-2.elasticbeanstalk.com


project moved to https://bitbucket.org/simplygetsoftware/ss_dev 25/12/2016 