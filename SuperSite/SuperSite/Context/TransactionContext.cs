﻿using SuperSite.Models;

using System.Data.Entity;


namespace SuperSite.Context
{
  //[DbConfigurationType(typeof(MySql.Data.Entity.MySqlEFConfiguration))]
    public class TransactionContext : DbContext
    {
        public DbSet<Transaction> Transactions { get; set; }

        public DbSet<Stock> Stocks { get; set; }

        public DbSet<Supplier> Suppliers { get; set; }

        public DbSet<PayPalCheckoutInfo> PayPalCheckoutInfoes { get; set; }

    }
}