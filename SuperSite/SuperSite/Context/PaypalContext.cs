﻿using SuperSite.Models;

using System.Data.Entity;


namespace SuperSite.Context
{
  //[DbConfigurationType(typeof(MySql.Data.Entity.MySqlEFConfiguration))]
    public class PayPalContext : DbContext
    {  
        public DbSet<PayPalCheckoutInfo> PayPalCheckoutInfoes { get; set; }
       
        public DbSet<Stock> Stocks { get; set; }
    }
}