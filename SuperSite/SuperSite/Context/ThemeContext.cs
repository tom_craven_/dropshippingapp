﻿using SuperSite.Models;

using System.Data.Entity;


namespace SuperSite.Context
{
 //[DbConfigurationType(typeof(MySql.Data.Entity.MySqlEFConfiguration))]
    public class ThemeContext : DbContext
    {
        public DbSet<Theme> Themes
        { get; set; }
   }
}