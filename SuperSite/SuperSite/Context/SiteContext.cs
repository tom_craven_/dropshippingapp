﻿using SuperSite.Models;

using System.Data.Entity;


namespace SuperSite.Context
{
 // [DbConfigurationType(typeof(MySql.Data.Entity.MySqlEFConfiguration))]
    public class SiteContext : DbContext
    {
        public DbSet<Site> Sites { get; set; }

        public DbSet<Theme> Themes { get; set; }

        public DbSet<Stock> Stocks { get; set; }
      
    }
}