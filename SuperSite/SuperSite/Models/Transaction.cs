﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.ComponentModel;

namespace SuperSite.Models
{
    public class Transaction
    {       
        [DisplayName("Transaction-UID#")]     
        public int TransactionId { set; get; }
        
        [DisplayName("Stock-UID#")]
        public int StockId { get; set; }
        
        public virtual Stock Stock { get; set; }
   
        public DateTime IPNDateRecieved { get; set; }    

        [DisplayName("PayPalCheckoutInfo")]
        public virtual PayPalCheckoutInfo PayPalCheckoutInfo { get; set; }

        public Transaction(PayPalCheckoutInfo payPalCheckoutInfo)
        {          
           this.TransactionId = payPalCheckoutInfo.TransactionId;                                
           this.StockId = Convert.ToInt16(payPalCheckoutInfo.item_number);
           this.IPNDateRecieved = DateTime.Now;
        }
        public Transaction() { }

    }

}