﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace SuperSite.Models
{
    public class Theme
    {   [Key]
        [DisplayName("Theme-UID")]
        public int ThemeId { set; get; }
        [DisplayName("File Name")]
        public String CSSFileName { set; get; }
        [DisplayName("Theme Color")]
        public String ThemeName { set; get; }

        public String Colour { get; set; }

        public virtual List<Site> sites { get; set; }

    }
}