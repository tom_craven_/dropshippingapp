﻿using SuperSite.Context;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Web;

namespace SuperSite.Models
{
   
    public class PayPalListenerModel : BaseModel
    {
        public PayPalCheckoutInfo _PayPalCheckoutInfo { get; set; }
        StreamWriter streamOut;
        StreamReader streamIn;
        public void GetStatus(byte[] parameters)
        {

            //verify the transaction             
            string status = Verify(true, parameters);

            if (status == "VERIFIED")
            {
                //UPDATE YOUR DATABASE

                //TextWriter txWriter = new StreamWriter(Server.MapPath("../uploads/") + Session["orderID"].ToString() + ".txt");
                //txWriter.WriteLine(strResponse);
                //txWriter.Close();

                //check the payment_status is Completed
                //check that txn_id has not been previously processed
                //check that receiver_email is your Primary PayPal email
                //check that payment_amount/payment_currency are correct
                //process payment
                if (_PayPalCheckoutInfo.payment_status.ToLower() == "completed")
                {
                    //TransactionContext db = new TransactionContext();
                    //db.PayPalCheckoutInfo.Add(_PayPalCheckoutInfo);
                    //db.SaveChanges();
                    //check that txn_id has not been previously processed to prevent duplicates                      

                    //check that receiver_email is your Primary PayPal email                                          

                    //check that payment_amount/payment_currency are correct                       

                    //process payment/refund/etc   

                }
            }
            else if (status == "INVALID")
            {
                //UPDATE YOUR DATABASE

                //TextWriter txWriter = new StreamWriter(Server.MapPath("../uploads/") + Session["orderID"].ToString() + ".txt");
                //txWriter.WriteLine(strResponse);
                ////log for manual investigation
                //txWriter.Close();
            }
            else
            {  //UPDATE YOUR DATABASE

                //TextWriter txWriter = new StreamWriter(Server.MapPath("../uploads/") + Session["orderID"].ToString() + ".txt");
                //txWriter.WriteLine("Invalid");
                ////log response/ipn data for manual investigation
                //txWriter.Close();
            }
        }
        private string Verify(bool isSandbox, byte[] parameters)
        {

            string response = "";
            try
            {

                string url = isSandbox ?
                  "https://www.sandbox.paypal.com/cgi-bin/webscr" : "https://www.paypal.com/cgi-bin/webscr";

                HttpWebRequest webRequest = (HttpWebRequest)WebRequest.Create(url);
                webRequest.Method = "POST";
                webRequest.ContentType = "application/x-www-form-urlencoded";

                //must keep the original intact and pass back to PayPal with a _notify-validate command
                string data = Encoding.ASCII.GetString(parameters);
                data += "&cmd=_notify-validate";

                webRequest.ContentLength = data.Length;

                //Send the request to PayPal and get the response                 
                using (streamOut = new StreamWriter(webRequest.GetRequestStream(), System.Text.Encoding.ASCII))
                {
                    streamOut.Write(data);                    
                }

                using (streamIn = new StreamReader(webRequest.GetResponse().GetResponseStream()))
                {
                    response = streamIn.ReadToEnd();
                   
                }
                Dispose();
            }
            catch { }

            return response;

        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
        protected virtual void Dispose(bool disposing)
        {
            if (disposing)
            {
                streamOut.Close();
                streamIn.Close();
            }
           
        }
    }


}