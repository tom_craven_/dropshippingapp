﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.ComponentModel;

namespace SuperSite.Models
{
    
    public class Supplier
    {   [Key]
        [DisplayName("Supplier-UID#")]
        public int SupplierId { set; get; }

        [DisplayName("Supplier")]
        public String Name { set; get; }

        [DisplayName("Our customer reference")]
        public String Reference { set; get; }

        [DisplayName("Pass Phrase")]
        public String PassPhrase { set; get; }

        [DisplayName("Contact Email")]
        public string POEmail { get; set; }

        public virtual List<Stock> Stocks { get; set; }

    }
}