﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace SuperSite.Models
{
    public class Stock
    {
        [Key]
        [DisplayName("Stock-UID#")]
        public int StockId { set; get; }
        [DisplayName("Stock Name")]
        public string Name { set; get; }
        [DisplayName("Retail value")]
        public double Price { set; get; }
        [DisplayName("Cost value")]
        public double Cost { set; get; }
        [DisplayName("Paypal Button Id")]
        public string PaypalButtonId { set; get; }

        public virtual List<Transaction> Transactions { get; set; }
        public virtual List<Site> SiteConfigurations { get; set; }

        [DisplayName("Supplier-UID#")]
        public int SupplierId { get; set; }
        [DisplayName("Supplier")]
        public virtual Supplier Supplier { get; set; }

        [DisplayName("Main Image")]
        public String Filename1 { get; set; }
        [DisplayName("Minor Image")]

        public String Filename2 { get; set; }
        [DisplayName("Collection#1")]
        public String Filename3 { get; set; }

        [DisplayName("Collection#2")]
        public String Filename4 { get; set; }

        [DisplayName("Collection#3")]
        public String Filename5 { get; set; }

        [DisplayName("Collection#4")]
        public String Filename6 { get; set; }

        [DisplayName("Collection#5")]
        public String Filename7 { get; set; }

        [DisplayName("Collection#6")]
        public String Filename8 { get; set; }

        [DisplayName("Collection#7")]
        public String Filename9 { get; set; }

        [DisplayName("Banner Image")]
        public String Filename10 { get; set; }

    }
}