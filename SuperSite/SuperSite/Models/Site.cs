﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SuperSite.Models
{
    public class Site
    {
        [Key]
        [DisplayName("Stock-UID#")]
        public int SiteId { set; get; }
        [DisplayName("Description")]
        public string Title { set; get; }
        [DisplayName("Status")]
        public int isActive { set; get; }

        [DisplayName("Theme-UID#")]
        public int ThemeId { get; set; }
        public virtual Theme Theme { get; set; }

        [DisplayName("Stock-UID#")]
        public int StockId { get; set; }
        public virtual Stock Stock { get; set; }
        [DisplayName("Main Image")]
        public String Filename1 { get; set; }
        [DisplayName("SlideShow-1")]
        public String Filename2 { get; set; }
        [DisplayName("SlideShow-2")]
        public String Filename3 { get; set; }
        [DisplayName("SlideShow-3")]
        public String Filename4 { get; set; }
        [DisplayName("SlideShow-4")]
        public String Filename5 { get; set; }
        [DisplayName("Feature-1")]
        public String Filename6 { get; set; }
        [DisplayName("Feature-2")]
        public String Filename7 { get; set; }
        [DisplayName("Feature-3")]
        public String Filename8 { get; set; }
        [DisplayName("Requirement")]
        public String Filename9 { get; set; }
        [DisplayName("Checkout")]
        public String Filename10 { get; set; }
        [DisplayName("Footer")]
        public String Filename11 { get; set; }
       


    }
}