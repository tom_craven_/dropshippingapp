﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(SuperSite.Startup))]
namespace SuperSite
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
