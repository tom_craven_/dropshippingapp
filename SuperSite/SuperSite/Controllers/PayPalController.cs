﻿using SuperSite.Context;
using SuperSite.Models;
using System;
using System.Linq;
using System.Net;
using System.Web.Mvc;

namespace SuperSite.Controllers
{
    public class PayPalController : Controller
    {
        TransactionContext db = new TransactionContext();
        PayPalContext pp = new PayPalContext();
        
        public ActionResult Index()
        {

            return View(db.Stocks.ToList());
        }

        public ActionResult Success() {
            return View();
        }
        public ActionResult Details(int? id)
        {
            {
                if (id == null)
                    return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
                PayPalCheckoutInfo payPalCheckoutInfo = pp.PayPalCheckoutInfoes.Find(id);
                if (payPalCheckoutInfo == null)
                {
                    return HttpNotFound();
                }
                return View(payPalCheckoutInfo);
            }
        }

        [AllowAnonymous]
        public EmptyResult PayPalPaymentNotification(PayPalCheckoutInfo payPalCheckoutInfo)
        {
            try
            {
               
                if (ModelState.IsValid)
                {
                    Response.AppendToLog("Final");
                    PayPalListenerModel model = new PayPalListenerModel();
                    model._PayPalCheckoutInfo = payPalCheckoutInfo;
                    byte[] parameters = Request.BinaryRead(Request.ContentLength);

                    if (parameters != null)
                    {
                        model.GetStatus(parameters);
                    }
                    pp.PayPalCheckoutInfoes.Add(payPalCheckoutInfo);
                    pp.SaveChanges();

                    Transaction Transaction = new Transaction(payPalCheckoutInfo); 
                    db.Transactions.Add(Transaction);
                    db.SaveChanges();                   
                }               
            }
            catch(Exception ex)
            {
                Response.AppendToLog("Invalid IPN model recieved");
                Response.AppendToLog(ex.ToString());
            }
             
            
            return new EmptyResult();
        }
    }
}