﻿using SuperSite.Context;
using SuperSite.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SuperSite.Controllers
{
    public class HomeController : Controller
    {
        public Site activeTheme { get; set; }
        public void GetThemeAndSet()
        {
            SiteContext Sitedb = new SiteContext();
            activeTheme = Sitedb.Sites
                   .Where(b => b.isActive == 1)
                   .FirstOrDefault();
            if (activeTheme == null)
            {
                activeTheme = new Site();
                activeTheme.Theme = new Theme();
                activeTheme.Theme.CSSFileName = "css/style-blue.css";
            }
        }

        public ActionResult Index()
        {
            GetThemeAndSet();
            ViewBag.CSSFileName = activeTheme.Theme.CSSFileName;
            return View(activeTheme);
        }

        public ActionResult About()
        {
            ViewBag.Message = "";

            return View("Home");
        }
       
        public ActionResult Contact()
        {
            ViewBag.Message = "Engineered by Tom";

            return View();
        }

        public ActionResult GoToHome()
        {
            return View("Home");
        }
       
    }
}