﻿using SuperSite.Context;
using SuperSite.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;

namespace SuperSite.Controllers
{
    [Authorize]
    public class StockController : Controller
    {
        public Site activeTheme { get; set; }
        ThemeContext themeDb = new ThemeContext();
        StockContext db = new StockContext();

        public void GetThemeAndSet()
        {
            SiteContext Sitedb = new SiteContext();
            activeTheme = Sitedb.Sites
                   .Where(b => b.isActive == 1)
                   .FirstOrDefault();
            if (activeTheme == null)
            {
                activeTheme = new Site();
                activeTheme.Theme = new Theme();
                activeTheme.Theme.CSSFileName = "css/style-blue.css";
            }
        }
              
        // GET: Stock
        public ActionResult Index()
        {                          
                return View(db.Stocks.ToList());    
        }
        public ActionResult CustomerIndex() {        
            return View("ShopFront", db.Stocks.ToList());
        }

        // GET: Stock/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
                Stock stock = db.Stocks.Find(id);
                if (stock == null)
                {
                    return HttpNotFound();
                }
                return View(stock);       
        }

        // GET: Stock/Create
        public ActionResult Create()
        {
            ViewData["Suppliers"] = db.Suppliers.ToList();
            return View();
        }

        // POST: Stock/Create
        [HttpPost]
        public ActionResult Create(Stock stock)
        {
            try
            {
                stock.PaypalButtonId = "N/A";
                if (ModelState.IsValid)
                {
                    var fileName = "";
                    var path = "";

                    var file1 = Request.Files[0];
                    if (file1 != null && file1.ContentLength > 0)
                    {
                        fileName = Path.GetFileName(file1.FileName);
                        path = Path.Combine(Server.MapPath("~/img/"), fileName);
                        file1.SaveAs(path);
                        stock.Filename1 = fileName;
                    }

                    var file2 = Request.Files[1];
                    if (file2 != null && file2.ContentLength > 0)
                    {
                        fileName = Path.GetFileName(file2.FileName);
                        path = Path.Combine(Server.MapPath("~/img/"), fileName);
                        file2.SaveAs(path);
                        stock.Filename2 = fileName;
                    }
                    var file3 = Request.Files[2];
                    if (file3 != null && file3.ContentLength > 0)
                    {
                        fileName = Path.GetFileName(file3.FileName);
                        path = Path.Combine(Server.MapPath("~/img/"), fileName);
                        file3.SaveAs(path);
                        stock.Filename3 = fileName;
                    }
                    var file4 = Request.Files[3];
                    if (file4 != null && file4.ContentLength > 0)
                    {
                        fileName = Path.GetFileName(file4.FileName);
                        path = Path.Combine(Server.MapPath("~/img/"), fileName);
                        file4.SaveAs(path);
                        stock.Filename4 = fileName;
                    }
                    var file5 = Request.Files[4];
                    if (file5 != null && file5.ContentLength > 0)
                    {
                        fileName = Path.GetFileName(file5.FileName);
                        path = Path.Combine(Server.MapPath("~/img/"), fileName);
                        file5.SaveAs(path);
                        stock.Filename5 = fileName;
                    }
                    var file6 = Request.Files[5];
                    if (file6 != null && file6.ContentLength > 0)
                    {
                        fileName = Path.GetFileName(file6.FileName);
                        path = Path.Combine(Server.MapPath("~/img/"), fileName);
                        file6.SaveAs(path);
                        stock.Filename6 = fileName;
                    }
                    var file7 = Request.Files[6];
                    if (file7 != null && file7.ContentLength > 0)
                    {
                        fileName = Path.GetFileName(file7.FileName);
                        path = Path.Combine(Server.MapPath("~/img/"), fileName);
                        file7.SaveAs(path);
                        stock.Filename7 = fileName;
                    }
                    var file8 = Request.Files[7];
                    if (file8 != null && file8.ContentLength > 0)
                    {
                        fileName = Path.GetFileName(file8.FileName);
                        path = Path.Combine(Server.MapPath("~/img/"), fileName);
                        file8.SaveAs(path);
                        stock.Filename8 = fileName;
                    }
                    var file9 = Request.Files[8];
                    if (file9 != null && file9.ContentLength > 0)
                    {
                        fileName = Path.GetFileName(file9.FileName);
                        path = Path.Combine(Server.MapPath("~/img/"), fileName);
                        file9.SaveAs(path);
                        stock.Filename9 = fileName;
                    }
                    var file10 = Request.Files[9];
                    if (file10 != null && file10.ContentLength > 0)
                    {
                        fileName = Path.GetFileName(file10.FileName);
                        path = Path.Combine(Server.MapPath("~/img/"), fileName);
                        file10.SaveAs(path);
                        stock.Filename10 = fileName;
                    }                
                    db.Stocks.Add(stock);
                    db.SaveChanges();
                    return RedirectToAction("Index");
                }
                return View(stock);
            }
            catch
            {
                return View();
            }
        }

        // GET: Stock/Edit/5
        public ActionResult Edit(int? id)
        {
            ViewData["Suppliers"] = db.Suppliers.ToList();
            if (id == null)
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            Stock stock = db.Stocks.Find(id);
            if (stock == null)
            {
                return HttpNotFound();
            }
            return View(stock);
        }

        // POST: Stock/Edit/5
        [HttpPost]
        public ActionResult Edit(Stock stock)
        {
            try
            {
                if (ModelState.IsValid)
                {

                    db.Entry(stock).State = System.Data.Entity.EntityState.Modified;
                    db.SaveChanges();
                    return RedirectToAction("Index");
                }
                return View(stock);
            }
            catch
            {
                return View();
            }
        }

        // GET: Stock/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            Stock stock = db.Stocks.Find(id);
            if (stock == null)
            {
                return HttpNotFound();
            }
            return View(stock);
        }

        // POST: Stock/Delete/5
        [HttpPost]
        public ActionResult Delete(int? id, Stock stockk)
        {
            try
            {
                Stock stock = new Stock();
                if (ModelState.IsValid)
                {
                    if (id == null)
                    {
                        return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
                    }
                    stock = db.Stocks.Find(id);
                    if (stock == null)
                    {
                        return HttpNotFound();
                    }
                    db.Stocks.Remove(stock);
                    db.SaveChanges();
                    return RedirectToAction("Index");
                }
                return View(stock);
            }
            catch
            {
                return View();
            }
        }
    }
}
