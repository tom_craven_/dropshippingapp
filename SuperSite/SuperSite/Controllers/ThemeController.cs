﻿using SuperSite.Context;
using SuperSite.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;

namespace SuperSite.Controllers
{
    [Authorize]
    public class ThemeController : Controller
    {

        ThemeContext db = new ThemeContext();

        // GET: Theme
        public ActionResult Index()
        {
            return View(db.Themes.ToList());
        }

        // GET: Theme/Details/5
        public ActionResult Details(int? id)
        {
            if (id==null)
            return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            Theme Theme = db.Themes.Find(id);
            if (Theme == null)
            {
                return HttpNotFound();
            }return View(Theme);
               
        }

        // GET: Theme/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Theme/Create
        [HttpPost]
        public ActionResult Create(Theme Theme)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    db.Themes.Add(Theme);
                    db.SaveChanges();
                    return RedirectToAction("Index");
                }
                return View(Theme);
            }
            catch
            {
                return View();
            }
        }

        // GET: Theme/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            Theme Theme = db.Themes.Find(id);
            if (Theme == null)
            {
                return HttpNotFound();
            }
            return View(Theme);
        }

        // POST: Theme/Edit/5
        [HttpPost]
        public ActionResult Edit(Theme Theme)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    db.Entry(Theme).State = System.Data.Entity.EntityState.Modified;
                    db.SaveChanges();
                    return RedirectToAction("Index");
                }
                return View(Theme);
            }
            catch
            {
                return View();
            }
        }

        // GET: Theme/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            Theme Theme = db.Themes.Find(id);
            if (Theme == null)
            {
                return HttpNotFound();
            }
            return View(Theme);
        }

        // POST: Theme/Delete/5
        [HttpPost]
        public ActionResult Delete(int? id, Theme Themee)
        {
            try
            {
                Theme Theme = new Theme();
                if (ModelState.IsValid)
                {
                    if (id == null)
                    {
                        return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
                    }
                    Theme = db.Themes.Find(id);
                    if (Theme == null)
                    {
                        return HttpNotFound();
                    }
                    db.Themes.Remove(Theme);
                    db.SaveChanges();
                    return RedirectToAction("Index");
                }
                return View(Theme);
            }
            catch
            {
                return View();
            }
        }
    }
}
