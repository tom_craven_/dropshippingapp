﻿using SuperSite.Context;
using SuperSite.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;

namespace SuperSite.Controllers
{
    [Authorize]
    public class SupplierController : Controller
    {
        public Site activeTheme { get; set; }

        StockContext db = new StockContext();

        // GET: Supplier
        public ActionResult Index()
        {
      
            return View(db.Suppliers.ToList());
        }

        // GET: Supplier/Details/5
        public ActionResult Details(int? id)
        {
            if (id==null)
            return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            Supplier Supplier = db.Suppliers.Find(id);
            if (Supplier == null)
            {
                return HttpNotFound();
            }return View(Supplier);
               
        }

        // GET: Supplier/Create
        public ActionResult Create()
        {
            
            return View();
        }

        // POST: Supplier/Create
        [HttpPost]
        public ActionResult Create(Supplier Supplier)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    db.Suppliers.Add(Supplier);
                    db.SaveChanges();
                    return RedirectToAction("Index");
                }
                return View(Supplier);
            }
            catch
            {
                return View();
            }
        }

        // GET: Supplier/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            Supplier Supplier = db.Suppliers.Find(id);
            if (Supplier == null)
            {
                return HttpNotFound();
            }
            return View(Supplier);
        }

        // POST: Supplier/Edit/5
        [HttpPost]
        public ActionResult Edit(Supplier Supplier)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    db.Entry(Supplier).State = System.Data.Entity.EntityState.Modified;
                    db.SaveChanges();
                    return RedirectToAction("Index");
                }
                return View(Supplier);
            }
            catch
            {
                return View();
            }
        }

        // GET: Supplier/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            Supplier Supplier = db.Suppliers.Find(id);
            if (Supplier == null)
            {
                return HttpNotFound();
            }
            return View(Supplier);
        }

        // POST: Supplier/Delete/5
        [HttpPost]
        public ActionResult Delete(int? id, Supplier Suppliere)
        {
            try
            {
                Supplier Supplier = new Supplier();
                if (ModelState.IsValid)
                {
                    if (id == null)
                    {
                        return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
                    }
                    Supplier = db.Suppliers.Find(id);
                    if (Supplier == null)
                    {
                        return HttpNotFound();
                    }
                    db.Suppliers.Remove(Supplier);
                    db.SaveChanges();
                    return RedirectToAction("Index");
                }
                return View(Supplier);
            }
            catch
            {
                return View();
            }
        }
    }
}
