﻿using SuperSite.Context;
using SuperSite.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;

namespace SuperSite.Controllers
{
    [Authorize]
    public class SiteController : Controller
    {
        public Site siteConfig { get; set; }

        SiteContext db = new SiteContext();

        public void GetThemeAndSet()
        {
            SiteContext Sitedb = new SiteContext();
            siteConfig = Sitedb.Sites
                   .Where(b => b.isActive == 1)
                   .FirstOrDefault();
            if (siteConfig == null)
            {
                siteConfig = new Site();
                siteConfig.Theme = new Theme();
                siteConfig.Theme.CSSFileName = "css/style-blue.css";
            }
        }

        public ActionResult MakeActive(int id)
        {        
                siteConfig = db.Sites
           .Where(b => b.isActive == 1)
           .FirstOrDefault();
                if (siteConfig ==  null)
                {
                siteConfig = db.Sites.Find(id);
                siteConfig.isActive = 1;
                db.Entry(siteConfig).State = System.Data.Entity.EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }else
            {
                siteConfig.isActive = 0;
                db.Entry(siteConfig).State = System.Data.Entity.EntityState.Modified;
                db.SaveChanges();
                Site siteTheme = db.Sites.Find(id);
                siteTheme.isActive = 1;
                db.Entry(siteTheme).State = System.Data.Entity.EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
               
        }
        public ActionResult CustomerIndex()
        {
            return View("ShopFront", db.Sites.ToList());
        }

        public ActionResult About()
        {
            ViewBag.Message = "";

            return View();
        }

        public ActionResult Index()
        {   
            return View(db.Sites.ToList());
        }


        // GET: Site/Details/5
        public ActionResult Details(int? id)
        {
            if (id==null)
            return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            Site site = db.Sites.Find(id);
            if (site == null)
            {
                return HttpNotFound();
            }return View(site);
               
        }

        // GET: Site/Create
        public ActionResult Create()
        {
            ViewData["Themes"] = db.Themes.ToList();
            ViewData["Stocks"] = db.Stocks.ToList();
            return View();
        }

        // POST: Site/Create
        [HttpPost]
        public ActionResult Create(Site siteTheme)
        {
            ViewData["Themes"] = db.Themes.ToList();
            ViewData["Stocks"] = db.Stocks.ToList();
            try
            {
                if (ModelState.IsValid)
                {
                    var fileName = "";
                    var path = "";

                    var file1 = Request.Files[0];
                    if (file1 != null && file1.ContentLength > 0)
                    {
                        fileName = Path.GetFileName(file1.FileName);
                        path = Path.Combine(Server.MapPath("~/img/"), fileName);
                        file1.SaveAs(path);
                        siteTheme.Filename1 = fileName;
                    }

                    var file2 = Request.Files[1];
                    if (file2 != null && file2.ContentLength > 0)
                    {
                        fileName = Path.GetFileName(file2.FileName);
                        path = Path.Combine(Server.MapPath("~/img/"), fileName);
                        file2.SaveAs(path);
                        siteTheme.Filename2 = fileName;
                    }
                    var file3 = Request.Files[2];
                    if (file3 != null && file3.ContentLength > 0)
                    {
                        fileName = Path.GetFileName(file3.FileName);
                        path = Path.Combine(Server.MapPath("~/img/"), fileName);
                        file3.SaveAs(path);
                        siteTheme.Filename3 = fileName;
                    }
                    var file4 = Request.Files[3];
                    if (file4 != null && file4.ContentLength > 0)
                    {
                        fileName = Path.GetFileName(file4.FileName);
                        path = Path.Combine(Server.MapPath("~/img/"), fileName);
                        file4.SaveAs(path);
                        siteTheme.Filename4 = fileName;
                    }
                    var file5 = Request.Files[4];
                    if (file5 != null && file5.ContentLength > 0)
                    {
                        fileName = Path.GetFileName(file5.FileName);
                        path = Path.Combine(Server.MapPath("~/img/"), fileName);
                        file5.SaveAs(path);
                        siteTheme.Filename5 = fileName;
                    }
                    var file6 = Request.Files[5];
                    if (file6 != null && file6.ContentLength > 0)
                    {
                        fileName = Path.GetFileName(file6.FileName);
                        path = Path.Combine(Server.MapPath("~/img/"), fileName);
                        file6.SaveAs(path);
                        siteTheme.Filename6 = fileName;
                    }
                    var file7 = Request.Files[6];
                    if (file7 != null && file7.ContentLength > 0)
                    {
                        fileName = Path.GetFileName(file7.FileName);
                        path = Path.Combine(Server.MapPath("~/img/"), fileName);
                        file7.SaveAs(path);
                        siteTheme.Filename7 = fileName;
                    }
                    var file8 = Request.Files[7];
                    if (file8 != null && file8.ContentLength > 0)
                    {
                        fileName = Path.GetFileName(file8.FileName);
                        path = Path.Combine(Server.MapPath("~/img/"), fileName);
                        file8.SaveAs(path);
                        siteTheme.Filename8 = fileName;
                    }
                    var file9 = Request.Files[8];
                    if (file9 != null && file9.ContentLength > 0)
                    {
                        fileName = Path.GetFileName(file9.FileName);
                        path = Path.Combine(Server.MapPath("~/img/"), fileName);
                        file9.SaveAs(path);
                        siteTheme.Filename9 = fileName;
                    }
                    var file10 = Request.Files[9];
                    if (file10 != null && file10.ContentLength > 0)
                    {
                        fileName = Path.GetFileName(file10.FileName);
                        path = Path.Combine(Server.MapPath("~/img/"), fileName);
                        file10.SaveAs(path);
                        siteTheme.Filename10 = fileName;
                    }
                    var file11 = Request.Files[10];
                    if (file11 != null && file11.ContentLength > 0)
                    {
                        fileName = Path.GetFileName(file11.FileName);
                        path = Path.Combine(Server.MapPath("~/img/"), fileName);
                        file11.SaveAs(path);
                        siteTheme.Filename11 = fileName;                    
                    }             
                    db.Sites.Add(siteTheme);
                    db.SaveChanges();
                    return RedirectToAction("Index");
                }
    
                return View(siteTheme);
            }
            catch
            {
                
                return View();
            }
        }

        // GET: Site/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            Site siteTheme = db.Sites.Find(id);
            if (siteTheme == null)
            {
                return HttpNotFound();
            }
            ViewData["Themes"] = db.Themes.ToList();
            ViewData["Stocks"] = db.Stocks.ToList();
            return View(siteTheme);
        }

        // POST: Site/Edit/5
        [HttpPost]
        public ActionResult Edit(Site siteTheme)
        {
            try
            {
                if (ModelState.IsValid)
                {                
                    db.Entry(siteTheme).State = System.Data.Entity.EntityState.Modified;
                    db.SaveChanges();
                    return RedirectToAction("Index");
                }
                return View(siteTheme);
            }
            catch
            {
                return View();
            }
        }

        // GET: Site/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            Site siteTheme = db.Sites.Find(id);
            if (siteTheme == null)
            {
                return HttpNotFound();
            }
            return View(siteTheme);
        }

        // POST: Site/Delete/5
        [HttpPost] //*Ignore siteThemee is only required to change the method signature, work with id instead*
        public ActionResult Delete(int? id, Site siteThemee)
        {
            try
            {
                Site siteTheme = new Site();
                if (ModelState.IsValid)
                {
                    if (id == null)
                    {
                        return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
                    }
                    siteTheme = db.Sites.Find(id);
                    if (siteTheme == null)
                    {
                        return HttpNotFound();
                    }
                    db.Sites.Remove(siteTheme);
                    db.SaveChanges();
                    return RedirectToAction("Index");
                }
                return View(siteTheme);
            }
            catch
            {
                return View();
            }
        }
    }
}
